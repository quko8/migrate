source "$HOME/.venvs/default/bin/activate.fish"

alias docker="sudo docker"
alias dnf="sudo dnf"

alias nodm="sudo systemctl start nodm.service"

alias t="taskr"

set -x GOPATH $HOME/wrk/go
