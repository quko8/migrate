#!/usr/bin/env python3

# PEP8 isn't good here
# pylint: disable=E1101,C0413,C0111,C0301

from importlib.util import find_spec
from sys import executable, argv
from os import execv, environ
from os.path import abspath
from pip import main as pip_exec

if find_spec('sh') is None:
    pip_exec(['install', '--upgrade', 'pip'])
    pip_exec(['install', 'sh'])
    execv(executable, ['python3'] + argv)

import sh


USERNAME = 'dmitry'
USER_FULLNAME = 'Dmitry Vakhnenko'
USER_EMAIL = 'dmitry.vakhnenko@ya.ru'


RELEASE = sh.rpm('-E', '%fedora').strip()

HOME = environ.get('HOME')
DATA = abspath('./data')


def install(*packages):
    sh.sudo.dnf.install(*packages, _fg=True)


# repos
install(
    '--nogpgcheck',
    f'https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-{RELEASE}.noarch.rpm',
    f'https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-{RELEASE}.noarch.rpm',
    'http://mirror.yandex.ru/fedora/russianfedora/russianfedora/free/fedora/russianfedora-free-release-stable.noarch.rpm',
    'http://mirror.yandex.ru/fedora/russianfedora/russianfedora/nonfree/fedora/russianfedora-nonfree-release-stable.noarch.rpm',
    'http://mirror.yandex.ru/fedora/russianfedora/russianfedora/fixes/fedora/russianfedora-fixes-release-stable.noarch.rpm'
)

# enable updates-testing
sh.sudo.dnf('config-manager', '--set-enabled', 'updates-testing', _fg=True)

# upgrade
sh.sudo.dnf.clean.all(_fg=True)
sh.sudo.dnf.upgrade(_fg=True)

# video
install('pciutils')

VIDEO_PACKAGES = [
    'glx-utils', 'mesa-dri-drivers', 'plymouth-system-theme', 'xorg-x11-drv-libinput',
    'xorg-x11-server-Xorg', 'xorg-x11-utils', 'xorg-x11-xauth', 'xorg-x11-xinit'
]

devices = sh.lspci('-mm')
if 'AMD/ATI' in devices:
    VIDEO_PACKAGES.append('xorg-x11-drv-ati')
else:
    print('Video device not detected')

install(*VIDEO_PACKAGES)


# audio
install('alsa-plugins-pulseaudio')

# flow
install('nano', 'tree', 'git', 'htop', 'fish', 'fpaste')
# install('docker', 'docker-compose')

# devel
install('gcc', 'redhat-rpm-config', 'python3-devel', 'openssl-devel')

# i3
install('nodm', 'i3', 'i3status', 'i3lock', 'dmenu', 'xdg-utils')

# apps
install('telegram-desktop', 'chromium')

# games
install('snes9x-gtk')

# hack font
sh.sudo.dnf.copr.enable('heliocastro/hack-fonts', _fg=True)
install('hack-fonts')

# dirs
sh.mkdir(
    f'{HOME}/.config/fish',
    f'{HOME}/.config/i3',
    f'{HOME}/.config/X11',
    parents=True)

# nodm
sh.sudo.cp(f'{DATA}/nodm/nodm.conf', '/etc/nodm.conf', _fg=True)
sh.sudo.cp(f'{DATA}/nodm/nodm.pam', '/etc/pam.d/nodm', _fg=True)

# fix fonts
sh.sudo.cp(f'{DATA}/local.conf', '/etc/fonts/local.conf', _fg=True)

# i3
sh.cp(f'{DATA}/i3/config', f'{HOME}/.config/i3/config')
sh.cp(f'{DATA}/i3/status', f'{HOME}/.config/i3/status')

# Xresources
sh.cp(f'{DATA}/xresources', f'{HOME}/.config/X11/xresources')

# fish
sh.cp(f'{DATA}/fish/fishd.fedora', f'{HOME}/.config/fish/fishd.fedora')
sh.sudo.usermod('-s', '/usr/bin/fish', USERNAME, _fg=True)
sh.cp(f'{DATA}/fish/config.fish', f'{HOME}/.config/fish/config.fish')

# git
sh.git.config('--global', 'user.name', USER_FULLNAME)
sh.git.config('--global', 'user.email', USER_EMAIL)
sh.git.config('--global', 'core.editor', 'nano')

# audio
sh.sudo.usermod('-aG', 'audio', 'dmitry', _fg=True)
# unmute

# disable useless servicesThere is no Internet connection
sh.sudo.systemctl.disable('firewalld', _fg=True)
sh.sudo.systemctl.disable('avahi-daemon', _fg=True)

print('OK')
